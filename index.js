let userName;
let userAge;

while (!userName || !userAge || !isFinite(userAge)) {
  userName = prompt("What's your name?", userName);
  userAge = prompt("How old are you?", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website.");
} else if (userAge >= 18 && userAge <= 22) {
  let okay = confirm("Are you sure you want to continue?");

  switch (okay) {
    case true:
      alert(`Welcome, ${userName}`);
      break;

    case false:
      alert("You are not allowed to visit this website.");
      break;
    default:
  }
} else if (userAge > 22) {
  alert(`Welcome, ${userName}`);
}
